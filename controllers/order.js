const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');

// Controller function to purchase a product
module.exports.purchaseProduct = async (req, res) => {
  try {

    if(req.user.isAdmin) {
    return res.send({message: "Action Forbidden"})
    }
    // Extract necessary information from the request
    const { productId, quantity } = req.body;
    const userId = req.user.id;

    // Fetch the product price from the database
    const products = await Product.findById(productId);
    if (!products) {
      return res.status(404).json({ error: 'Product not found' });
    }

    const price = products.price;
    const totalAmount = price * quantity;

    // Create a new order
    const order = new Order({
      userId,
      products: [{ productId, quantity, price }],
      totalAmount
    });

    // Save the order in the database
    await order.save();

    return res.status(200).json({
      message: 'Purchase Successful',
      totalAmount
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Internal server error' });
  }
};



// Controller function to get all orders of a user (Admin)
module.exports.getAllOrders = async (req, res) => {
  const { userId } = req.params;

  try {
    // Check if the user is an admin
    if (!req.user || req.user.role == 'isAdmin') {
      return res.status(403).json({ success: false, message: 'Access denied' });
    }

    // Query the database to fetch all orders of the user
    const orders = await Order.find();

    res.json({ orders });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};


// Controller function to get all orders of a user (User)
module.exports.getUserOrders = async (req, res) => {
  const userId  = req.user.id;

  try {
    // Query the database to fetch all orders of the user
    const orders = await Order.find({ userId })


    res.json({ orders });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
};




















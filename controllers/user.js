// Dependencies and Modules
const User = require("../models/User");
const bcrypt = require('bcrypt');
const Product = require("../models/Product");
const Order = require("../models/Order");
const auth = require('../auth');



module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		// The "find" metho
		// returns a record if a match is found.
		if(result.length > 0) {
			return true; //"Duplicate email found"
		} else {
			return false;
		}
	})
};

// User Registration Function
/*
	Business Logic:
		1. Create a new user object using the mongoose model and the information from the request body.
		2. Make sure that the password to be restored is encrypted.
		3. Save the new User to the database.
*/

module.exports.registerUser = (reqBody) => {

	// Create a new variable newUser and instantiates a new User object using the mongoose model.
	// Uses the information from the request body to provide all the necessary information about the User object.
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 - is the numnber of salt rounds thath bcrypt algorithm will run in order to encrypt the password.
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error) {
			return false;
		// User registration successful
		} else {
			return true;
		}
	// catch() code block will handle other kinds of error.
	// this one prevents our app from crashing when an error occured in the backend server.
	}).catch(err => err)
};


// User Authentication Function
/*
	Business Logic:
		1. Check the database if the user email exists.
		2. Compare the password provided in the login from the password stored in the database.
		3. If yes, generate/return a JSON web token if the user is successfuly logged in and return false if not.
*/

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		// User does not exist
		if (result == null) {
			return false
		} else {
			// Created the isPasswordCorrect variable to return the result of comparing the login form password an the data base password.
			// compareSync() method is used to comapre the non-encrypted password from the login form to the encrypted password from the database.
				// will return either true of false
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {
			// Generate an access token
			// Uses the "createAccessToken" method defined in the th auth.js file.
			// Returing an object back to the frontend application is a common practice to ensure that information is properly labled and real world examples normally return more complex information represented by objects.
				return res.send({access: auth.creatAcessToken(result)})
			}
		}
	}).catch(err => res.send(err));
};


// User Details Function
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = ''
		return res.send(result);
	}).catch(err => res.send(err))
};


// Controller method for updating a user as an admin
module.exports.updateUserAsAdmin = async (req, res) => {
  try {
    const { userId } = req.body;

    // Check if the provided user ID exists
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Update the user as an admin
    user.isAdmin = true;
    await user.save();

    return res.json({ message: 'User updated as admin successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};


 // Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
};


//[SECTION] Activate a course
module.exports.activateUser = (req, res) => {

	let updateAdminField = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(req.params.userId, updateAdminField)
	.then((course, error) => {

		//course archived successfully
		if(error){
			return res.send(false)

		// failed
		} else {
			return res.send(true)
		}
	})
	.catch(err => res.send(err))

};








 



















// Dependencies and Modules
const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");


// Create a product function
module.exports.createProduct = (req, res) => {

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})


	return newProduct.save().then((product, error) => {
		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	}).catch(err => res.send(err))
};



// Retrieve all products
/*
	Business Logic:
		1. Retrieve all the prdoucts from the prdoucts collection from the database.
*/

// We used the find methond of our Product Model.
// Because our Product model is connected to our product collection
// Course.find({}) is the same as db.course.find({})
// empty {} will return all the document from the courses collection
module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		return res.send(result);
	})
	.catch(err => res.send(err))
};



// Retrieve all ACTIVE products
/*
	Business Logic:
		1. Retrieve all the products from the database with property of "isActive" equals to true.
*/

module.exports.getAllActive = (req, res) => {
	return Product.find({ isActive : true }).then(result => {
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Retrieving a single product
/*
	Business Logic:
		1. Retrieve a single product from the products collection based on th document's object ID that we received from the url params.
*/
module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Update a Product
/*
	Business Logic:
		1. Create an "update" variable and use the properties from the mongoose model to pass the new values.
		2. Save the updatedProduct back into the product collection. Find the object first by its id before saving the changes.
		3. Return a true respond if saving is successful and return false if not.
*/
module.exports.updateProduct = (req, res) => {
	// Specify the fields/properties of the document to be updated
	let updateProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	// Syntax:
		// findByIdandUpdate(documentId, updatesToBeApplied)
	return Product.findByIdAndUpdate(req.params.productId, updateProduct).then((product, error) => {
		// Product not updated
		if(error) {
			return res.send(false);
		// Product updated successfully
		} else {
			return res.send(true);
		}
	}) 
	.catch(err => res.send(err))
};

// Archive Product

module.exports.archiveProduct = (req, res) => {

	let archiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(req.params.productId, archiveProduct).then((product, error) => {

		// console.log(course)
		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};



// Activate Product

module.exports.activateProduct = (req, res) => {

	let activateProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(req.params.productId, activateProduct).then((product, error) => {

		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
};


// Controller action to search for products by product name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      name: { $regex: productName, $options: 'i' } // case insensitive
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Controller action to search products based on the price
module.exports.searchProductByPriceRange = async (req, res) => {
  const { minPrice, maxPrice } = req.body;

  try {
    const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    res.json(products);
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};















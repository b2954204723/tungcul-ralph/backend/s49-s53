// Mongoose Dependency
const mongoose = require('mongoose');


// Schema/Blueprint
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'First Name is Required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});




// Model
module.exports = mongoose.model('User', userSchema);





